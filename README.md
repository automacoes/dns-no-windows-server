# Automatizar a criação de DNS pelo PowerShell

### Sem variáveis
```
Add-DnsServerPrimaryZone -Name claudiodz.com -ZoneFile claudiodz.com.DNS -DynamicUpdate NonsecureAndSecure
Add-DnsServerResourceRecordA -Name clienta -ZoneName claudiodz.com -AllowUpdateAny -ipAddress 192.168.1.11
```
### Com variáveis para serem substituídas na execução da pipeline
```
Add-DnsServerPrimaryZone -Name %dominio% -ZoneFile %dominio%.DNS -DynamicUpdate NonsecureAndSecure
Add-DnsServerResourceRecordA -Name $subdominio -ZoneName $dominio -AllowUpdateAny -ipAddress $ip
```

**Variáveis para execução da Pipeline**
- tipo=dominio / subdominio
- dominio='claudios.com'
- subdominio='cliente1'
- ip='192.168.1.11'

### Método para executar o comando PowerShell pelo CMD
```
C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe Start-Process -FilePath powershell.exe -ArgumentList 'COMANDO1 ; COMANDO2' -verb RunAs
```



### Extra Criar CNAME
```
Add-DnsServerResourceRecordCName -Name clientb -HostnameAlias teste.claudiodz.com -ZoneName claudiodz.com
```

```
Add-DnsServerPrimaryZone -NetworkID 192.168.1.0/24 -ZoneFile 140.168.192.in-addr.arpa
```

**Habilitar SSH no Windows**
```
Get-WindowsCapability -Online | ? Name -like 'OpenSSH*'
```

Fonte:
- https://ss64.com/ps/syntax-elevate.html
